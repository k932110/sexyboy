# hello world 100회 출력
for i in range(100):
    print("Hello World")

# 1 ~ 10까지 들어가는 리스트 변수
list_A = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(list_A)

# 딕셔너리변수['name']시 이름을 출력하는 딕셔너리 변수
dict_var = {
    "name" : "희성"
}
print(dict_var['name'])

settingID = "doctor@gmail.com"
settingPW = "1234"

def LoginChecker(id, password):
    if(settingID == id and settingPW == password):
        return True
    else:
        return False

isLogin = LoginChecker("doctor@gmail.com", "1234")

if isLogin:
    print("로그인 성공")
else:
    print("로그인 실패")