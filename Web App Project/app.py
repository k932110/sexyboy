from flask import Flask, request, render_template
from gameFunction import cash_multiply
import loginModule

app = Flask(__name__)

# 메인 화면
@app.route('/')
def hello():
    return render_template("main.html")

# 자산 관리 페이지
@app.route('/money')
def money():
    return render_template("money.html")

# 어떤 방법으로 데이터를 주든 'GET', 'POST' 전부 쓸 수 있다
@app.route('/show', methods=['GET', 'POST'])
def show():
    if request.method == 'GET':
        return "GET으로 들어온 페이지"
    else:
        money = request.form["money"]
        money = cash_multiply(int(money)) # 돈 곱해주는 함수
        return "돈이 3배로 복사가 되버린다고? <br><b>Your C@in ({})".format(money)

# 회원가입
@app.route('/signup', methods=['GET', 'POST']) 
def signup():
    if request.method == 'GET':
        return render_template("signup.html")
    else:
        user_name = request.form["user_name"]
        user_email = request.form["user_email"]
        user_pwd = request.form["user_pwd"]
        return "회원 정보 <br>NAME : {}<br>EMAIL : {}<br>PASSWORD : {}".format(user_name, user_email, user_pwd)

# 로그인
@app.route('/signin', methods=['GET', 'POST']) 
def signin():
    if request.method == 'GET':
        return render_template("signin.html")
    else:
        user_email = request.form["user_email"]
        user_pwd = request.form["user_pwd"]
        #return "{} {}".format(user_email, user_pwd)

        isLogin = loginModule.LoginChecker(user_email, user_pwd)
        
        if(isLogin):
            return "Login Success"
        else:
            return "Login Failed"

@app.route('/showmoney')
def showmoney():
    # 따옴표 3개를 쓰면 여러줄로 쓸 수 있다.
    # 다음과 같이 HTML코드를 사용할 때 < 거의 사용하지 않는다.
    # 그래서 큰 HTML코드를 외부에서 불러 사용할 수 있게 해주는 게 있다.
    return '''
        <iframe width="420" height="345" src="https://www.youtube.com/embed/tgbNymZ7vqY">
        </iframe>
    '''
# render_template을 사용하여 HTML 코드들을 불러올 수 있다.
@app.route('/htmlShowmoney')
def showmoneyHTML():
    return render_template("youtube.html")

if __name__ == '__main__':
    app.run()